En esta documentación pueden encontrarse la definición de las estructuras principales que contiene los datos abiertos de Bicimad. 

Para acceder a la información de los datos estáticos publicados debes dirigirte a https://mobilitylabs.emtmadrid.es. 
En la opción de "OPEN DATA"->"ACCESO A CONTENIDOS"  podrás localizar los dos conjuntos de datos a los que hace referencia esta documentación dentro de la categoría de MOVILIDAD:

Situación de las Estaciones Hora a Hora: Cada sesenta minutos, el sistema almacena la información del estado de cada base de Bicimad 

Tracks anonimizados de usos y recorridos: Tracks de ruta de Bicimad anonimizados



